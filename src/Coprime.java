import java.util.Scanner;

public class Coprime {
    public static void main(String[] args) {
        int x,y,z,nod1, nod2, nod3;
        x = inputIntFromConsole("Enter the first value: ");
        y = inputIntFromConsole("Enter the second value: ");
        z = inputIntFromConsole("Enter the third value: ");
        nod1 = nod(x, y);
        nod2 = nod(z, y);
        nod3 = nod(x, z);
        if (nod1 != 1) {
            display(" aren't coprime");
        } else if (nod2 != 1) {
            display(" aren't coprime");
        } else if (nod3 != 1) {
            display(" aren't coprime");
        }else{
            display(" are coprime!");
        }
    }
    public static int inputIntFromConsole(String message) {
        int value;
        Scanner sc = new Scanner(System.in);
        System.out.print(message);
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.print("Incorrect value. " + message);
        }
        value = sc.nextInt();
        return value;
    }
    public static int nod(int x, int y) {
        while (x != 0 && y != 0) {
            if (x > y) { x = x % y; }
            else { y = y % x; }
        }
        return x + y;
    }
    public static void display(String message){
        System.out.println("Numbers" + message);
    }
}